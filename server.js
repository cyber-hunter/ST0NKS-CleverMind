const express = require('express'),
    apiRoutes = require('./api'),
    session = require('express-session'),
    cookieParser = require('cookie-parser'),
    ehandlebars = require('express-handlebars'),
     helpers = require('handlebars-helpers'),
    path = require('path'),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    db = require('./db/db'),
    axios = require('axios'),
    apiHandler = require('./utils/api_handler'),
    https = require('https');

const app = express();
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(session({
    secret: '142e6ecf42884f03',
    resave: false,
    saveUninitialized: true,
  }));

app.use(express.static(path.join(__dirname, 'static')));


app.use(cookieParser());

app.set('view engine', 'hbs');
app.use('/api', apiRoutes);//api endpoint
app.engine('hbs', ehandlebars({
  extname: 'hbs',
  defaultView: 'listazas',
  layoutsDir: path.join(__dirname, 'views/layouts'),
  partialsDir: path.join(__dirname, 'views/partials'),
  
  helpers: helpers(),
}));
app.use(morgan('tiny'));

app.get('/teszt_sca', (req, res) => {
  req.session.details = apiHandler.startSession((client_id, client_app_key, skill, nonce, session_id, status) =>{
    // const {nonce, session_id, status} = data;)
    // const {} = data;
    //console.log("NONCE:" + str(nonce))
    const message = JSON.stringify({
      client_id,
      'nonce': nonce,//nonce.toString(),
      skill,
      //step: "sca",
      data: {
        "psu_redirect_link": "http://localhost:8080/token_exchange"
      }
    })
     // 
     console.log((message))
    //  return
    //  axios.post( `https://api.finqware.com/v1/sessions/${session_id}/steps`, 
    //  message,{'headers': {"Content-Type": "application/json"}}
      
    //  ).then((data) => {
      //  console.log(data.data);
      //  return
       apiHandler.strongCustomerAuthentication(client_id, nonce, skill, session_id, 
         redirect_link='http://localhost:8080/token_exchange', (redirectURL)=>{
           // console.error("**" + JSON.stringify(data) +"**")
           res.redirect(redirectURL)
         })
       //res.send(data.data);
    //  }).catch((err) => {
    //    console.log("ERROR");
    //    console.log(err);
    //  })
  })
})

app.post('/start_session', urlencodedParser, (req, res)=>{
  const {iban, currency} = req.body;
  req.session.details = apiHandler.startSession((client_id, client_app_key, skill, nonce, session_id, status) =>{
    // const {nonce, session_id, status} = data;
     //const {client_id, client_secret} = apiHandler.getInfo();
    console.log(session_id);
    // return
    //return;
    const message = JSON.stringify({
      "client_id":client_id,
      "nonce":nonce,
      "skill":skill,
      "step": "account_input",
      "data": {
        "accounts":[//{ "iban": "RO65RZBR0000069999999950", "currency": "RON" },
        
           {"iban":iban, "currency":currency},
          //{ "iban": "RO10RZBR0000069999999970", "currency": "RON" }
        ]
      }
     });
     console.log((message));
     //return
     const options = {
      hostname: 'api.finqware.com',
      //port: 443,
      path: `/v1/sessions/${session_id}/steps`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': message.length
        }
      }
      
    // const req = https.request(options, response => {
    //   console.log(`statusCode: ${response.statusCode} on the first post`)
    
    //   res.on('data', d => {
    //     process.stdout.write(d)
    //     process.stdout.write('account input finished')
    //     apiHandler.strongCustomerAuthentication(client_id, d['nonce'], skill, redirect_link='https://google.com')
    //   })
    // })
    // req.on('error', error => {
    //   console.error(error)
    // })
    
    // req.write(message)
    // req.end()
    // return
    axios.post( `https://api.finqware.com/v1/sessions/${session_id}/steps`, 
      message,{'headers': {"Content-Type": "application/json"}}
       
      ).then((data) => {
        console.log(data);
        //return
        apiHandler.strongCustomerAuthentication(client_id, data.data['nonce'], skill, session_id, 
          redirect_link='http://localhost:8080/token_exchange', (redirectURL)=>{
            // console.error("**" + JSON.stringify(data) +"**")
            res.redirect(redirectURL)
          })
        //res.send(data.data);
      }).catch((err) => {
        console.log("ERROR");
        console.log(err);
      })
  });
  //req.session.temp_token = temp_token;
  //console.log(req.body);
  //res.send(temp_token + 'henlo');
})
//apiHandler.startSession();



app.get('/', function(req, res) {
  res.redirect('index.html');//sendFile(path.join(__dirname + '/index.html'));
});

app.get('/token_exchange', (req, res) =>{
  console.log(req.query)
  // return
  if(req.query.temp_token){
    const {client_id, client_secret}  = apiHandler.getInfo();
    const temp_token = req.query.temp_token
    req.session.temp_token = req.query.temp_token;
    axios.post('https://api.finqware.com/v1/tokens', JSON.stringify({
      client_id, client_secret, temp_token
    }),{'headers': {"Content-Type": "application/json"}
    }).then(response => {
      const {credentials_id, access_token} = response.data
      console.log(response.data);
      res.redirect('/')
      apiHandler.getAccountInfo(client_id, client_secret, credentials_id, access_token, (accInfo)=>{
        console.log(accInfo)
        db.addTransactions(req.session.name, accInfo)
      })
    }).catch(err => {
      console.log(err);
    })
  }else{
    res.send("MISSING PARAMETER temp_token")
  }
});


app.listen(8080, () => { console.log('Server listening at http://localhost:8080'); });
console.log(apiHandler.getInfo())