const MongoClient = require("mongodb").MongoClient;

let connectionString ;
{
    connectionString= "mongodb+srv://marci:Tatratea69@cluster0.tbexq.gcp.mongodb.net/<dbname>?retryWrites=true&w=majority";

}
var dbo;
// const client = new MongoClient(connectionString, {useUnifiedTopology: true});
MongoClient.connect(connectionString,{ useUnifiedTopology: true }, function(err, db)  {
    if(err){
         throw(err);
    }
    dbo = db.db("mydb");
    dbo.createCollection("Transactions").catch((err) => {})
    dbo.createCollection("CategorizedTransactions").catch(err => {})
})

module.exports.addTransaction = (owner, transactionID, date, spent) => {
    dbo.collection('Transactions').insertOne({
        '_id': transactionID,
        'owner': owner,
        'date': date,
        'spent': spent
    }).catch(err => {
        console.log(err)
    })
}
module.exports.addTransactions = (owner, transactions) => {
    transactions.forEach((o) => {
        this.addTransaction(owner, o.id, o.date, o.spent)
    })
}
module.exports.addTransactionCategory = (owner, transactionID, category) => {
    dbo.collection("CategorizedTransactions").insertOne({
        '_id': transactionID,
        'category': category,
        'owner': owner
    })
}

module.exports.getChartData = (owner, callback)=>{
    dbo.collection('Transactions').find({
        'owner': owner
    }, (err, transactions) => {
        
        if(err){
            console.log(err)
        }else{
            transactions.sort((a, b)=>{
                return new Date(b['date']) - new Date(a['date'])
            })
            result = {
                food: [],
                transportation: [],
                other: transactions
            }
            console.log(transactions)
            dbo.collection('CategorizedTransactions').find({'owner': owner}).forEach(
                ct => {
                    result[ct['category']].push(ct['_id']);
                    for (var i = 0; i < result.other.length; i++)
                        if (result.other[i] === ct['_id']) { 
                            result.other[i].splice(i, 1);
                            break;
                        }
                    console.log(result)
                    
                }
            )

            // callback(result)
        }
    })
}

// try {
//     client.connect();

    
 
// } catch (e) {
//     console.error(e);
// }

// client