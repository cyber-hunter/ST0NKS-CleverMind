const express = require('express'),
    morgan = require("morgan"),
    bodyParser = require('body-parser'),
    example = require('./example'),
    transactions = require('./transactions');


const router = express.Router();

router.use(morgan('tiny'));

router.use(bodyParser.json);

router.use("/example", example);
router.use("/transactions", transactions)

module.exports = router;